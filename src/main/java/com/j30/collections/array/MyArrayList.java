package com.j30.collections.array;

public class MyArrayList {
    private static final int INITIAL_ARRAY_SIZE = 10;
    private Object[] array;
    private int size = 0; // rozmiar listy, na poczatku 0


    public MyArrayList(){
        this(INITIAL_ARRAY_SIZE);

    }
    public MyArrayList(int initialArraySize) {
        array = new Object[initialArraySize];
    }

    //todo:
    // - dodawanie elementu (na koniec)     -
    // - listowanie elementow               - toString
    // - size
    // - dodanie elementu (na pozycję)
    // - usunięcie elementu z pozycji n

    public int size() {
        return size;
    }

    //  dodanie elementu (na koniec)
    public void add(Object element){
        checkSizeAndExtendIfNeeded(); //sprawdz rozmiar i rozszerz jesli trzeba
        array[size++] = element;
        //post-inkrementacja, najpierw wykona sie linia, a potem inkrementacja
    }

    private void checkSizeAndExtendIfNeeded() {
        if(size >= array.length) {
            //  1. Stworzyc tablice dwa razy wieksza
            Object[] newArray = new Object[array.length * 2];

            //  2. Przepisanie elementow
            for (int i = 0; i  < array.length; i++) {
                newArray[i] = array[i];

            }
            array = newArray; // nowa tablica, nie interesuje mnie stara tylko nowa tablica
        }
    }

    /**
     *  usun element na pozycji.
     *
     * @param indeks - pozycja z ktorej usuwamy
     */
    public void remove(int indeks) {
        if (indeks >= 0 && indeks < size) {
            for (int i = indeks; i < size - 1; i++) {
                array[i] = array [i +1];
            }
            array[size--] = null;   // post dekrementacja size = size -1

        }else {
            throw new IndexOutOfBoundsException("Invalid index: " + indeks);
        }
    }

    @Override
    public String toString() {
        //  lista -> 1 2 3 4
        //  [1, 2, 3, 4]
        //  lista -> "abc" "def" "gha"
        //  ["abc" "def" "gha"]

        StringBuilder sb = new StringBuilder("[");

        if(size > 0) {
            for (int i = 0; i < size; i++) {
                sb.append(array[i]);
                if (i != size - 1) {
                    sb.append(", ");
                }
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
