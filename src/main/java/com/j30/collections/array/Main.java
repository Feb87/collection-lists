package com.j30.collections.array;

public class Main {
    public static void main(String[] args) {
        MyArrayList list = new MyArrayList();

        list.add("abc");
        list.add("def");
        list.add(1);
        list.add(5.0);
        list.add(0.3);
        list.add(0.4);
        list.add(0.5);
        list.add(0.6);
        list.add(0.7);
        list.add(0.8);
        list.add(0.9);
        list.add(0.10);


        System.out.println(list);
        list.remove(8);
        System.out.println(list);
        list.remove(8);
        System.out.println(list);
        list.remove(8);
        System.out.println(list);
        list.remove(list.size() - 1);;
        System.out.println(list);
        System.out.println(list.size());

    }
}
